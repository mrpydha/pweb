<?php

class Admin extends CI_Controller{

    function __construct(){
        parent::__construct();

        if($this->session->userdata('status') != "login"){
            redirect(base_url("Controller"));
        }
    }

    function index(){
        $data=array(
            'data' => $this->M_login->orang()
        );
        $this->load->view('v_admin',$data);
    }
	function homepage(){
		$this->load->view('home');
	}
}
