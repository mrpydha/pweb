<!DOCTYPE html>
<html>
<head>
    <title>Home Tabunganku</title>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css' ?>">
    <script src="<?php echo base_url('assets/jquery-3.3.1.min.js'); ?>" charset="utf-8"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/web-fonts-with-css/css/fontawesome-all.min.css' ?>">
    <script src="<?php echo base_url().'assets/web-fonts-with-css/svg-with-js/js/fontawesome-all.min.js' ?>" charset="utf-8"></script>

</head> 
<style media="screen">

</style>
<body>
  <div class="nav-side-menu">
      <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
      <div class="menu-list">
          <ul id="menu-content" class="menu-content collapse out">
              <li>
                  <a href="#">
                  <a class="navbar-brand" href="<?php echo base_url().'Admin' ?>">
                    <h5><i class="fa fa-globe"></i> Home</h5>
                    </a>
                  </a>
              </li>
              <li data-toggle="collapse" data-target="#products" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Tabunganku </span></a>
              </li>
              <ul class="sub-menu collapse" id="products">
                  <li><a href="#">Setoran Akhiratku</a></li>
                  <li><a href="#">List Setoranku</a></li>
              </ul>
              <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i> New </span></a>
              </li>
              <ul class="sub-menu collapse" id="new">
                  <li>New New 1</li>

              </ul>
              <li>
                  <a href="#">
                      <i class="fa fa-user fa-lg"></i> Profile
                  </a>
              </li>
          </ul>
      </div>
  </div>
  <!-- MAIN CONTENT -->

  <div class="container rounded shadow py-0 m-2" id="main" style="width:calc(100% - 200px)">
    <div class="navbar navbar-expand-lg navbar-dark bg-dark rounded shadow p-1 m-0" >
      </a>
      <div class="mr-sm-2 navbar-nav ml-auto" style="float:right">
        <a href="<?php echo base_url(); ?>Controller/logout" style="color:white"><h5><i class="fa fa-users"></i> Logout</h5></a>
      </div>

    </div>
      <div class="row" style="margin-left: -10px">
          <div class="col-lg-12 mx-2    ">

            <h1 >Selamat Datang, <?php echo $this->session->userdata("nama"); ?></h1>


<table class="table rounded shadow"  >
  <thead>
    <tr>
      <th>Id Santri</th>
      <th>Nama Santri</th>
      <th>Surah</th>
      <th>Ayat</th>
      <th>Aksi</th>

    </tr>
  </thead>
<tbody>
<?php foreach ($data as $dt){ ?>
  <tr>
    <td><?php echo $dt['id_santri'];?></td>
    <td><?php echo $dt['nama_santri']; ?></td>
    <td><?php echo $dt['surah']; ?></td>
    <td><?php echo $dt['ayat']; ?></td>
    <td>
    <a href='#' class="btn btn-info"> Ubah</a>
    <a href='#' calss="btn btn-danger"> Hapus</a>
    </td>
  </tr>
<?php } ?>
</tbody>
</table>




    <script src="<?php echo base_url().'assets/js/bootstrap.js' ?>" charset="utf-8"></script>

</body>
</html>
